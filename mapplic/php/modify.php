<?php

	// function modify
	//connect to database
	require 'connection.php';

	function updatePosition($ID){
		require 'connection.php';
		//sql statement
		//get map data
		$query = "SELECT `post_content` FROM `wp_posts` WHERE `ID`= ".$ID."; ";
		//get the result and store it in {result}
		$result = $conn->query($query);

		if ($result->num_rows > 0) {
			// output data of each row
			while($row = $result->fetch_assoc()) {
				//decode post_content into json object and save it in {map_json}
				$map_json = json_decode($row['post_content']);
				//get levels array and save it in {levels}
				$levels = $map_json->levels;
				//select first level and save it in {level1}
				$level1 = $levels[0];
				//get locations array as save it in {locations_level1}
				$locations_level1 = $level1->locations;
				//loop through the array
				foreach ($locations_level1 as $location) {
					//modify the data
					if($location->id == $_POST['location_id']) {
						$location->x = $_POST['location_x'];
					}
				}
				//encode $map_json to json string
				$map_json_encoded = json_encode($map_json);

				//update the `post_content` with the updated json
				$query = "UPDATE `wp_posts` SET `post_content` = '".$map_json_encoded."' WHERE `ID` = ".$ID."; ";
				$conn->query($query);
			}
		} else {
			echo "0 results";
		}
		$conn->close();

	}

function updateStatus($ID){
	require 'connection.php';
	//sql statement
	//get map data
	$query = "SELECT `post_content` FROM `wp_posts` WHERE `ID`= ".$ID."; ";
	//get the result and store it in {result}
	$result = $conn->query($query);

	if ($result->num_rows > 0) {
		// output data of each row
		while($row = $result->fetch_assoc()) {
			//decode post_content into json object and save it in {map_json}
			$map_json = json_decode($row['post_content']);
			//get levels array and save it in {levels}
			$levels = $map_json->levels;

			foreach ($levels as $level) {
				$locations = $level->locations;
				foreach ($locations as $location) {
					//modify the data
					if($location->id == $_POST['location_id']) {
						if (isset($location->lightstatus)){
							$location->lightstatus = $_POST['location_status'];
						}
					}
				}
			}
			//encode $map_json to json string
			$map_json_encoded = json_encode($map_json);

			//update the `post_content` with the updated json
			$query = "UPDATE `wp_posts` SET `post_content` = '".$map_json_encoded."' WHERE `ID` = ".$ID.";";
			$conn->query($query);
		}
	}
}

function checkUpdate($ID){
	require 'connection.php';
	//sql statement
	//get map data
	$query = "SELECT `post_content` FROM `wp_posts` WHERE `ID`= ".$ID."; ";
	//get the result and store it in {result}
	$result = $conn->query($query);

	if ($result->num_rows > 0) {
		// output data of each row
		while($row = $result->fetch_assoc()) {
			//decode post_content into json object and save it in {map_json}
			$map_json = json_decode($row['post_content']);
			//get levels array and save it in {levels}
			$levels = $map_json->levels;

			foreach ($levels as $level) {
				$locations = $level->locations;
				foreach ($locations as $location) {
					if (isset($location->lightstatus)){
						echo "id: ".$location->id.", status:".$location->lightstatus."\n";
					}
				}
			}
		}
	}
}

	// updatePosition();
	updateStatus(48);
	checkUpdate(48);

?>
